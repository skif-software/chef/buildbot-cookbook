user 'buildbot'

buildbot_master 'buildmaster' do
  user    'buildbot'
  group   'buildbot'
  basedir '/tmp/buildmaster'
  action [:install, :configure, :run]
end

buildbot_worker 'localworker' do
  user  'buildbot'
  group 'buildbot'
  local true
end

buildbot_worker 'remoteworker' do
  user     'buildbot'
  group    'buildbot'
  basedir  '/tmp/buildworker'
  password 'password'
  master_host 'localhost'
  master_port '9989'
  admin_info  'admin'
  host_info   'localhost'
end
