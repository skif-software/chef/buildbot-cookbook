name             'buildbot'
version          '0.4.0'
maintainer       'Serge A. Salamanka'
maintainer_email 'salamanka.serge@gmail.com'
license          'Apache-2.0'
description      'Installs and configures Buildbot'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url       'https://gitlab.com/skif-software/chef/buildbot-cookbook'
issues_url       'https://gitlab.com/skif-software/chef/buildbot-cookbook/issues'

chef_version     '>= 12.14' if respond_to?(:chef_version)

supports 'debian'
supports 'ubuntu'

depends 'poise-python'
