#
# Cookbook:: buildbot
# Resource:: master
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Installs and configures buildbot master

resource_name :buildbot_master

default_action :install

property :install_method, kind_of: String, default: 'package'
property :packages,       kind_of: Array,  default:  [
                                                      'buildbot',
                                                      'buildbot-www',
                                                      'buildbot-waterfall-view',
                                                      'buildbot-console-view',
                                                      'buildbot-grid-view'
                                                    ]
property :version,        kind_of: String, default: '1.4.0'
property :user,           kind_of: String, required: true
property :group,          kind_of: String, required: true
property :basedir,        kind_of: String, required: true
property :options,        kind_of: Array,  default:  []
property :variables,      kind_of: Hash,   default:  {}
property :parameters,     kind_of: Hash,   default:  {}
property :template_source, kind_of: String,   default: 'master.cfg.erb'
property :template_cookbook, kind_of: String, default: 'buildbot'

action :install do
  case new_resource.install_method
  when 'package'
    # Install the latest available version of Python 2
    # http://docs.buildbot.net/current/manual/installation/requirements.html
    python_runtime '3'
    # Install Buildmaster python packages
    # http://docs.buildbot.net/current/manual/installation/installation.html
    python_package new_resource.packages do
      action  :install
      version new_resource.version
    end
    # Create Buildmaster
    # http://docs.buildbot.net/current/manual/installation/buildmaster.html
    execute 'buildbot_create-master' do
      command "buildbot create-master #{new_resource.options.join(' ')} #{new_resource.basedir}"
      user   new_resource.user
      group  new_resource.group
      action :run
    end
  end
end

action :configure do
  # Create Buildmaster configuration
  # http://docs.buildbot.net/current/manual/configuration.html
  template ::File.join(new_resource.basedir, 'master.cfg') do
    source   new_resource.template_source
    cookbook new_resource.template_cookbook
    user   new_resource.user
    group  new_resource.group
    variables(new_resource.variables)
    action :create
    notifies :run, 'execute[buildbot_reconfig]'
  end
  execute 'buildbot_reconfig' do
    command "buildbot reconfig #{new_resource.basedir}"
    user   new_resource.user
    group  new_resource.group
    action :nothing
  end
end

action :run do
  # TODO: Setup Buildmaster service
  # service new_resource.name do
  #   #
  # end
  # Start Buildmaster
  execute 'buildbot_start' do
    command "buildbot start #{new_resource.basedir}"
    user   new_resource.user
    group  new_resource.group
    action :run
  end
end

action :upgrade do
  # Upgrade the existing Buildmaster
  # http://docs.buildbot.net/current/manual/installation/buildmaster.html#upgrading-an-existing-buildmaster
  execute 'buildbot_upgrade-master' do
    command "buildbot upgrade-master #{new_resource.basedir}"
    user   new_resource.user
    group  new_resource.group
    action :run
    live_stream true # print any errors or deprecation warnings that occur
  end
end
