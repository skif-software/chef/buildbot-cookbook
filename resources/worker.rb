#
# Cookbook:: buildbot
# Resource:: worker
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Installs and configures buildbot worker

resource_name :buildbot_worker

default_action :install

property :install_method, kind_of: String, default: 'package'
property :packages,       kind_of: Array,  default:  ['buildbot-worker']
property :version,        kind_of: String, default: '1.4.0'
property :options,        kind_of: Array,  default:  []
property :user,           kind_of: String
property :group,          kind_of: String
property :basedir,        kind_of: String
property :password,       kind_of: String
property :master_host,    kind_of: String
property :master_port,    kind_of: String
property :admin_info,     kind_of: String
property :host_info,      kind_of: String
property :local,          kind_of: TrueClass

# Workaround to validate required properties
# https://github.com/chef/chef/issues/6505
def after_created
  [ :user,
    :group,
    :basedir,
    :password,
    :master_host,
    :master_port,
    :admin_info,
    :host_info ].each do |name|
    raise ArgumentError, "Property #{name} is not defined in the resource #{self}" unless property_is_set?(name)
  end unless property_is_set?(:local)
end # https://www.rubydoc.info/gems/chef/Chef%2FResource:after_created

action :install do
  case new_resource.install_method
  when 'package'
    # Install the latest available version of Python 2
    # http://docs.buildbot.net/current/manual/installation/requirements.html
    python_runtime '3'
    # Install Buildworker python packages
    # http://docs.buildbot.net/current/manual/installation/installation.html
    python_package new_resource.packages do
      action  :install
      version new_resource.version
    end
    # Create Buildworker
    # http://docs.buildbot.net/current/manual/installation/worker.html#creating-a-worker
    unless property_is_set?(:local)
      execute 'buildbot_create-worker' do
        command "buildbot-worker create-worker #{new_resource.options.join(' ')}" +
                                             " #{new_resource.basedir}" +
                                             " #{new_resource.master_host}:#{new_resource.master_port}" +
                                             " #{new_resource.name}" +
                                             " #{new_resource.password}"
        user   new_resource.user
        group  new_resource.group
        action :run
      end
      file ::File.join(new_resource.basedir, 'info', 'admin') do
        content new_resource.admin_info
        mode    '0644'
        owner   new_resource.user
        group   new_resource.group
        action :create
      end
      file ::File.join(new_resource.basedir, 'info', 'host') do
        content new_resource.host_info
        mode    '0644'
        owner   new_resource.user
        group   new_resource.group
        action :create
      end
    end
  end
end

action :run do
  # TODO
end
